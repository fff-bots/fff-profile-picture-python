import unittest
from fff_profile_picture import Generator
from PIL import Image


class GeneratorTest(unittest.TestCase):
    def test_processing_no_overlay_specified(self):
        generator = Generator(Image.open("tests/testimage.JPG"))
        self.assertRaises(Exception, generator.process().save("tests/test_processing_no_overlay_specified.png"))

    def test_processing_with_overlay_specified(self):
        generator = Generator(Image.open("tests/testimage.JPG"), overlay=Image.open("tests/nolobbydeal.png"))
        self.assertRaises(Exception, generator.process().save("tests/test_processing_with_overlay_specified.png"))


if __name__ == '__main__':
    unittest.main()
